package com.company;

public class Human {
      private String name;
      private int weight;

     public int getWeight() {
          return this.weight;
     }
      public void setWeight (int weight) {
         if (weight < 0 || weight > 500) {
         weight = 0;
     }
         this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
